import React, {useEffect} from 'react';
import {View, Text, Alert} from 'react-native';
import FlashMessage from 'react-native-flash-message';
import {MiniProject1Provider} from './src/context/Context_MiniProjek1';
import Routes from './src/routes';
import {
  AccountEdit,
  Intro,
  Login,
  MiniProjek1,
  Splashscreen,
  Tugas1,
  Tugas2,
  Tugas3,
  Tugas4,
} from './src/screens';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';

export default function App() {
  // appleID=13900058-348f-4a88-b45f-715a00c24839
  var firebaseConfig = {
    apiKey: 'AIzaSyCwpsiSRCJbv8695-pWxotaY0kZ0JZ489I',
    authDomain: 'sanbercode-bce23.firebaseapp.com',
    projectId: 'sanbercode-bce23',
    storageBucket: 'sanbercode-bce23.appspot.com',
    messagingSenderId: '707291033897',
    appId: '1:707291033897:web:dbf2a51ca8cd37a6dfc738',
    measurementId: 'G-BBDN9SMTR3',
  };
  // Inisialisasi firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('13900058-348f-4a88-b45f-715a00c24839', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync(
      {
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      SyncStatus,
    );

    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const SyncStatus = (status) => {
    switch (key) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Cheking for update');
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('Downloading package');
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to date');
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing update');
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notifaction', 'Update Installed');
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('Awaiting user');
      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log('onReceived => notification', notification);
  };

  const onOpened = (openResult) => {
    console.log('onOpened => openResult', openResult);
  };

  const onIds = (device) => {
    console.log('onIds => device', device);
  };

  return (
    <>
      {/* <Tugas1 /> */}
      {/* <Tugas2 /> */}
      {/* <Tugas3 /> */}
      {/* <Tugas4 /> */}
      {/* <MiniProject1Provider>
        <MiniProjek1 />
      </MiniProject1Provider> */}
      {/* <Splashscreen /> */}
      {/* <AccountEdit /> */}
      <Routes />
      <FlashMessage position="bottom" />
    </>
  );
}
