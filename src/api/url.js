const Url = {
  Api: 'https://crowdfunding.sanberdev.com/api',
  Home: 'https://crowdfunding.sanberdev.com',
};

export default Url;
