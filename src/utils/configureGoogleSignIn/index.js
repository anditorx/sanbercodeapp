import {GoogleSignin} from '@react-native-community/google-signin';

const configureGoogleSignIn = () => {
  GoogleSignin.configure({
    webClientId:
      '707291033897-d1vqpjt73vldp5dq4uckjac6n3eil72m.apps.googleusercontent.com',
  });
};
export default configureGoogleSignIn;
