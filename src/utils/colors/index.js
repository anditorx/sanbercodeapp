const mainColors = {
  white: 'white',
  black: '#000000',
  blackLowOppacity: 'rgba(0,0,0,0.5)',
  greyDark: '#858587',
  greyLight: '#dfe4ea',
  primary: '#FFD541',
  blue1: '#1297E0',
  blue2: '#0984E3',
  blue: '#0599ED',
  lightBlue: '#98C8DB',
  body: '#dfe4ea',
};

export const colors = {
  primary: mainColors.primary,
  body: mainColors.body,
  white: mainColors.white,
  black: mainColors.black,
  greyDark: mainColors.greyDark,
  greyLight: mainColors.greyLight,
  blue: mainColors.blue,
  blue2: mainColors.blue2,
  lightBlue: mainColors.lightBlue,
  blackLowOppacity: mainColors.blackLowOppacity,
  text: {
    default: mainColors.black,
    grey: mainColors.greyDark,
  },
  button: {
    disable: {
      background: mainColors.greyDark,
      text: mainColors.greyDark,
    },
    primary: {
      background: mainColors.primary,
      text: mainColors.white,
    },
  },
  loadingBackground: mainColors.blackLowOppacity,
};
