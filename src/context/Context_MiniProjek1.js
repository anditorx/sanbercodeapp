import React, {useState, createContext} from 'react';

export const MiniProject1Context = createContext();

export const MiniProject1Provider = (props) => {
  const [contact, setContact] = useState([]);

  return (
    <MiniProject1Context.Provider value={[contact, setContact]}>
      {props.children}
    </MiniProject1Context.Provider>
  );
};
