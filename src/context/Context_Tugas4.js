import React, {createContext} from 'react';

const Tugas4Context = createContext();

export const Tugas4Provider = Tugas4Context.Provider;
export const Tugas4Consumer = Tugas4Context.Consumer;

export default Tugas4Context;
