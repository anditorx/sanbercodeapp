import React, {useState} from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import {ImgHeroSlider02, ImgHeroSlider01} from '../../../assets';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const Slider = () => {
  const [slider, setSlider] = useState([
    ImgHeroSlider02,
    ImgHeroSlider01,
    ImgHeroSlider02,
    ImgHeroSlider01,
  ]);

  return (
    <SliderBox
      style={{
        paddingLeft: 20,
        height: 200,
        borderRadius: 10,
        width: windowWidth - 40,
        justifyContent: 'center',
        alignSelf: 'center',
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
      }}
      autoplay
      sliderBoxHeight={200}
      circleLoop
      disableOnPress={true}
      images={slider}
    />
  );
};

export default Slider;

const styles = StyleSheet.create({
  paginBox: {
    position: 'relative',
    bottom: 0,
    padding: 0,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    padding: 0,
    margin: 0,
    backgroundColor: 'white',
  },
  imgComp: {borderRadius: 15, width: '97%', marginTop: 5},
});
