import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors} from '../../../utils';

const Separator = () => {
  return <View style={styles.wrapper}></View>;
};

export default Separator;

const styles = StyleSheet.create({
  wrapper: {
    borderBottomWidth: 5,
    borderBottomColor: colors.greyLight,
  },
});
