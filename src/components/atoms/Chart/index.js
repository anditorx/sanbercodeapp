import React, {useState} from 'react';
import {processColor, StyleSheet, Text, View} from 'react-native';

import {BarChart} from 'react-native-charts-wrapper';
import {colors} from '../../../utils';
const Chart = ({type}) => {
  const data = [
    {y: 100},
    {y: 60},
    {y: 90},
    {y: 45},
    {y: 67},
    {y: 32},
    {y: 150},
    {y: 70},
    {y: 40},
    {y: 89},
  ];
  const month = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  const [legend, setLegend] = useState({
    enabled: false,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5,
  });

  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          values: data,
          label: '',
          config: {
            colors: [processColor(colors.primary)],
            stackLabels: [],
            drawFilled: false,
            drawValues: false,
          },
        },
      ],
    },
  });

  const [xAxis, setAxis] = useState({
    valueFormatter: month,
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: data.length - 0.4,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1}],
  });

  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  return (
    <BarChart
      style={{flex: 1}}
      data={chart.data}
      xAxis={xAxis}
      yAxis={yAxis}
      chartDescription={{text: ''}}
      legend={legend}
      doubleTapToZoomEnabled={false}
      pinchZoom={false}
      marker={{
        enabled: true,
        textSize: 14,
      }}
    />
  );
};

export default Chart;

const styles = StyleSheet.create({});
