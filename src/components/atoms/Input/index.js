import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import {color} from 'react-native-reanimated';
import {IconEyeHide, IconEyeOn} from '../../../assets';
import {colors} from '../../../utils/colors';

const Input = ({
  label,
  secureTextEntry,
  type,
  placeholder,
  value,
  onChangeText,
  onPress,
  disable,
  icon,
  text,
}) => {
  const [border, setBorder] = useState(colors.greyLight);
  const onFocusForm = () => {
    setBorder(colors.primary);
  };
  const onBlurFrom = () => {
    setBorder(colors.greyLight);
  };
  const [date, setDate] = useState('');

  if (type === 'number') {
    return (
      <View>
        <Text style={styles.label(border)}>{label}</Text>
        <TextInput
          keyboardType="numeric"
          onFocus={onFocusForm}
          style={styles.input(border)}
          onBlur={onBlurFrom}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
          placeholder={placeholder}
        />
      </View>
    );
  }
  if (type === 'input-icon') {
    return (
      <View style={styles.wrapper}>
        <Image source={icon} style={styles.icon1} />
        <TextInput
          onFocus={onFocusForm}
          style={styles.input2}
          placeholder={placeholder}
          onBlur={onBlurFrom}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
        />
      </View>
    );
  }

  if (type === 'input-icon-password') {
    return (
      <View style={styles.wrapper}>
        <Image source={icon} style={styles.icon1} />
        <TextInput
          onFocus={onFocusForm}
          style={styles.input2}
          placeholder={placeholder}
          onBlur={onBlurFrom}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
        />

        <TouchableOpacity onPress={onPress}>
          <Image
            source={secureTextEntry ? IconEyeHide : IconEyeOn}
            style={styles.iconPass}
          />
        </TouchableOpacity>
      </View>
    );
  }

  if (type === 'textarea') {
    return (
      <View>
        <Text style={styles.label(border)}>{label}</Text>
        <TextInput
          onFocus={onFocusForm}
          style={styles.textarea(border)}
          placeholder={placeholder}
          numberOfLines={5}
          multiline={true}
          onBlur={onBlurFrom}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
        />
      </View>
    );
  }

  if (type === 'search-home') {
    return (
      <View>
        <TextInput
          onFocus={onFocusForm}
          style={styles.input_search}
          onBlur={onBlurFrom}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
          placeholder={placeholder}
        />
      </View>
    );
  }

  return (
    <View>
      <Text style={styles.label(border)}>{label}</Text>
      <TextInput
        onFocus={onFocusForm}
        style={styles.input(border)}
        onBlur={onBlurFrom}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        editable={!disable}
        selectTextOnFocus={!disable}
        placeholder={placeholder}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  label: (border) => ({
    color: border,
    fontSize: 15,
  }),
  input: (border) => ({
    marginTop: 5,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
  }),
  input_search: {
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: colors.white,
    borderRadius: 10,
    color: colors.white,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.greyLight,
    borderRadius: 10,
    padding: 5,
    width: '100%',
  },
  input2: {
    flex: 1,
  },

  icon1: {
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    height: 25,
    width: 25,
  },
  iconPass: {
    resizeMode: 'contain',
    padding: 10,
    marginLeft: 10,
    marginRight: 20,
    height: 8,
    width: 11,
  },
  textarea: (border) => ({
    // backgroundColor: 'red',
    marginTop: 5,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
  }),
});
