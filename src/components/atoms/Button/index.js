import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {color} from 'react-native-reanimated';
import {IconFingerpringBlack, IconGoogleColor} from '../../../assets';
import {colors} from '../../../utils';

const Button = ({type, title, onPress, bgCol, check}) => {
  if (type === 'signin-google') {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: colors.greyLight,
          borderRadius: 10,
          paddingVertical: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={onPress}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={IconGoogleColor}
            style={{height: 24, width: 24, marginRight: 20}}
          />
          <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              color: colors.black,
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  if (type === 'signin-fingerprint') {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: colors.greyLight,
          borderRadius: 10,
          paddingVertical: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={onPress}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={IconFingerpringBlack}
            style={{height: 24, width: 24, marginRight: 20}}
          />
          <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              color: colors.black,
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  if (type === 'btn-login-intro') {
    return (
      <TouchableOpacity style={styles.btnLoginIntro} onPress={onPress}>
        <Text style={styles.txtBtnLoginIntro}>{title}</Text>
      </TouchableOpacity>
    );
  }

  if (type === 'btn-label-donation-1') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: 50,
          width: 100,
          backgroundColor: bgCol,
          elevation: 5,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12}}>{title}</Text>
      </TouchableOpacity>
    );
  }
  if (type === 'btn-label-donation-2') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: 50,
          width: 100,
          backgroundColor: bgCol,
          elevation: 5,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12}}>{title}</Text>
      </TouchableOpacity>
    );
  }
  if (type === 'btn-label-donation-3') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: 50,
          width: 100,
          backgroundColor: bgCol,
          elevation: 5,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12}}>{title}</Text>
      </TouchableOpacity>
    );
  }
  if (type === 'btn-label-donation-4') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: 50,
          width: 100,
          backgroundColor: bgCol,
          elevation: 5,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12}}>{title}</Text>
      </TouchableOpacity>
    );
  }
  if (type === 'btn-label-donation-5') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: 50,
          width: 100,
          backgroundColor: bgCol,
          elevation: 5,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12}}>{title}</Text>
      </TouchableOpacity>
    );
  }
  if (type === 'btn-label-donation-6') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: 50,
          width: 100,
          backgroundColor: bgCol,
          elevation: 5,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12}}>{title}</Text>
      </TouchableOpacity>
    );
  }

  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.txtBtn(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: type === 'not-active' ? colors.greyLight : colors.primary,
    paddingVertical: 10,
    borderRadius: 10,
  }),
  txtBtn: (type) => ({
    color: type === 'not-active' ? colors.greyDark : colors.white,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  }),
  btnLoginIntro: {
    backgroundColor: 'black',
    paddingVertical: 10,
    borderRadius: 10,
  },
  txtBtnLoginIntro: {
    color: colors.white,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
