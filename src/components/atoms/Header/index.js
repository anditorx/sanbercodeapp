import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import {
  IconBackBlack,
  IconBackColor,
  IconBackWhite,
  IconEditWhite,
  IconLikeWhite,
  IconSaveWhite,
  ImgAva,
} from '../../../assets';
import {colors} from '../../../utils';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Header = ({type, title, onPress, onPressBack, onPressSave, img}) => {
  if (type === 'arrow-back-bg-transparent') {
    return (
      <TouchableOpacity style={styles.headerTransparent} onPress={onPress}>
        <Image source={IconBackColor} style={{height: 24, width: 24}} />
      </TouchableOpacity>
    );
  }

  if (type === 'btn-back-and-save') {
    return (
      <View style={styles.headerRow}>
        <TouchableOpacity onPress={onPressBack}>
          <Image source={IconBackWhite} style={{height: 24, width: 24}} />
        </TouchableOpacity>
        <View>
          <Text style={styles.txtHeader}>{title}</Text>
        </View>
        <TouchableOpacity onPress={onPressSave}>
          <Image source={IconSaveWhite} style={{height: 24, width: 24}} />
          <Text style={{color: colors.white}}>Save</Text>
        </TouchableOpacity>
      </View>
    );
  }

  if (type === 'btn-back-and-edit') {
    return (
      <View style={styles.headerRow}>
        <TouchableOpacity onPress={onPressBack}>
          <Image source={IconBackWhite} style={{height: 24, width: 24}} />
        </TouchableOpacity>
        <View>
          <Text style={styles.txtHeader}>{title}</Text>
        </View>
        <TouchableOpacity onPress={onPress}>
          <Image source={IconEditWhite} style={{height: 24, width: 24}} />
          <Text style={{color: colors.white}}>Edit</Text>
        </TouchableOpacity>
      </View>
    );
  }

  if (type === 'btn-back') {
    return (
      <View style={styles.headerRow}>
        <TouchableOpacity onPress={onPressBack}>
          <Image source={IconBackWhite} style={{height: 24, width: 24}} />
        </TouchableOpacity>
        <View>
          <Text style={styles.txtHeader}>{title}</Text>
        </View>
        <View></View>
      </View>
    );
  }

  if (type === 'header-transparent-btn-back') {
    return (
      <View style={styles.headerRowTransparent}>
        <TouchableOpacity onPress={onPressBack}>
          <Image source={IconBackWhite} style={{height: 24, width: 24}} />
        </TouchableOpacity>
        <View>
          <Text style={styles.txtHeader}>{title}</Text>
        </View>
        <View></View>
      </View>
    );
  }

  if (type === 'header-home') {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 20,
        }}>
        <TouchableOpacity>
          <Image
            source={img}
            style={{
              height: 40,
              width: 40,
              borderRadius: 40 / 2,
              borderWidth: 3,
              borderColor: colors.white,
              backgroundColor: colors.white,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            paddingLeft: 10,
            borderWidth: 1,
            borderColor: colors.white,
            borderRadius: 10,
            width: windowWidth / 1.75,
            height: 35,
            justifyContent: 'center',
          }}>
          <TextInput
            onTouchStart={() => alert('Move to search screen')}
            style={{
              height: 35,
              color: colors.white,
              fontSize: 12,
            }}
            value="Cari ... "
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={IconLikeWhite} style={{height: 35, width: 35}} />
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={styles.header}>
      <Text style={styles.txtHeader}>{title}</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.primary,
    height: 60,
    justifyContent: 'center',
    padding: 20,
  },
  headerRow: {
    backgroundColor: colors.primary,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  headerRowTransparent: {
    backgroundColor: 'transparent',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  editText: {
    justifyContent: 'flex-end',
  },
  headerTransparent: {
    backgroundColor: 'transparent',
    height: 60,
    padding: 20,
  },
  txtHeader: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
  },
});
