import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {IconCameraWhite, IconUploadWithCamera} from '../../../assets';
import {colors} from '../../../utils';
import {Gap} from '../../atoms';

const UploaPhoto = ({type, onPress, img, name}) => {
  // const [photo, setPhoto] = useState(ImgNoAva2);

  if (type === 'add-photo') {
    return (
      <TouchableOpacity style={styles.wrapper} onPress={onPress}>
        <Image source={img} style={styles.photo} />
        <View>
          <Image source={IconUploadWithCamera} style={styles.addPhoto} />
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <View style={styles.wrapper}>
      <Image source={img} style={styles.photo} />
      <Text style={styles.name}>{name}</Text>
    </View>
  );
};

export default UploaPhoto;

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  photo: {
    height: 150,
    width: 150,
    borderRadius: 150 / 2,
    borderWidth: 8,
    borderColor: colors.white,
  },
  addPhoto: {
    position: 'absolute',
    height: 50,
    width: 50,
    right: -80,
    bottom: 10,
  },
  name: {
    fontSize: 20,
    marginTop: 10,
    color: colors.black,
  },
  desc: {
    fontSize: 15,
    color: colors.black,
  },
});
