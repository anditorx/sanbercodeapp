import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import NumberFormat from 'react-number-format';
import {IconTrash} from '../../../assets';
import {colors} from '../../../utils';
import {Separator} from '../../atoms';

const List = ({text1, text2, index, onPress, type, text3}) => {
  if (type === 'list-riwayat') {
    return (
      <>
        <View style={styles.wrapperRowListRiwayat} key={index}>
          <NumberFormat
            value={text1.toString()}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'Rp'}
            renderText={(value) => (
              <Text
                style={{fontSize: 17, color: colors.black, fontWeight: 'bold'}}>
                {`Total ${value}`}
              </Text>
            )}
          />

          <Text style={{fontSize: 16}}>{text2}</Text>
          <Text style={{fontSize: 16}}>{text3}</Text>
        </View>
        <Separator />
      </>
    );
  }

  return (
    <View style={styles.wrapperRowList} key={index}>
      <View>
        <Text style={{fontSize: 12, color: colors.primary}}>{text2}</Text>
        <Text style={{fontSize: 16}}>{text1}</Text>
      </View>
      <TouchableOpacity onPress={onPress}>
        <Image source={IconTrash} style={{height: 24, width: 24}} />
      </TouchableOpacity>
    </View>
  );
};

export default List;

const styles = StyleSheet.create({
  wrapperRowList: {
    borderWidth: 1,
    borderColor: colors.greyDark,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
  wrapperRowListRiwayat: {
    borderColor: colors.greyDark,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
});
