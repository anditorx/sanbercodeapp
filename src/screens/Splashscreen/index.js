import React, {useState, useEffect, useRef} from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
} from 'react-native';
import {ImgLogo} from '../../assets';
import {colors} from '../../utils';
const height = Dimensions.get('window').height;
const Splashscreen = () => {
  const fadeOut = useRef(new Animated.Value(1)).current;
  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeOut, {
      toValue: 0,
      duration: 3000,
      useNativeDriver: false,
    }).start();
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  }, [fadeOut, fadeIn]);

  const transformY = fadeIn.interpolate({
    inputRange: [0, 1],
    outputRange: [height, -height / 2],
  });

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
        <Animated.View style={[styles.titleContainer, {opacity: fadeOut}]}>
          <Text style={styles.txtTitle}>CROWDFUNDING</Text>
        </Animated.View>
        <Animated.View
          style={[
            styles.logo,
            {opacity: fadeIn, transform: [{translateY: transformY}]},
          ]}>
          <Image style={styles.imgLogo} source={ImgLogo} />
        </Animated.View>
      </View>
    </SafeAreaView>
  );
};

export default Splashscreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtTitle: {
    color: colors.white,
    fontSize: 24,
    fontWeight: 'bold',
    paddingTop: 10,
  },
  logo: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  textLogo: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.white,
  },
  imgLogo: {
    height: 100,
    width: 100,
  },
});
