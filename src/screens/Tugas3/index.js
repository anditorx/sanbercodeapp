import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {IconPlus, IconTrash} from '../../assets';

const Tugas3 = () => {
  const [data, setData] = useState([]);
  const [form, setForm] = useState({
    tanggal: '',
    todo: '',
  });

  const [border, setBorder] = useState('#444447');
  const onFocusForm = () => {
    setBorder('#1297E0');
  };
  const onBlurFrom = () => {
    setBorder('#444447');
  };

  const onPressAdd = () => {
    setData([...data, form]);
    setForm({tanggal: '', todo: ''});
  };

  const onPressDelete = (index) => {
    console.log('delete index ke:', index);
    let newData = data;
    newData.splice(index, 1);
    setData([...newData]);
  };

  const getCurrentDate = () => {
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    return date + '/' + month + '/' + year; //format: dd/mm/yyyy;
  };

  return (
    <>
      <StatusBar backgroundColor="#1297E0" barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.header}>
          <Text style={styles.txtHeader}>Tugas 3</Text>
        </View>
        <View style={styles.body}>
          <Text>Todo List</Text>
          {/* Form Input */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View style={{flex: 1, marginRight: 20}}>
              <TextInput
                onFocus={onFocusForm}
                style={styles.input(border)}
                onBlur={onBlurFrom}
                placeholder="Input here"
                value={form.todo}
                onChangeText={(value) =>
                  setForm({tanggal: getCurrentDate(), todo: value})
                }
              />
            </View>
            <TouchableOpacity
              onPress={onPressAdd}
              style={{
                height: 50,
                width: 50,
                backgroundColor: '#1297E0',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 10,
              }}>
              <Image source={IconPlus} style={{height: 24, width: 24}} />
            </TouchableOpacity>
          </View>
          {/* LIST */}
          <View
            style={{
              flex: 1,
              marginTop: 50,
            }}>
            {data !== [] &&
              data.map((item, index) => (
                // console.log(index),
                <View
                  key={index}
                  style={{
                    borderWidth: 3,
                    borderColor: '#CDC7C7',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 10,
                    borderRadius: 10,
                    marginBottom: 10,
                  }}>
                  <View>
                    <Text style={{fontSize: 12, color: '#AAA6A6'}}>
                      {item.tanggal}
                    </Text>
                    <Text style={{fontSize: 16}}>{item.todo}</Text>
                  </View>
                  <TouchableOpacity
                    value={index}
                    onPress={() => onPressDelete(index)}>
                    <Image source={IconTrash} style={{height: 24, width: 24}} />
                  </TouchableOpacity>
                </View>
              ))}
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Tugas3;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#1297E0',
    height: 60,
    justifyContent: 'center',
    padding: 20,
  },
  txtHeader: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
  },
  body: {
    flex: 1,
    backgroundColor: '#dfe4ea',
    padding: 20,
  },
  input: (border) => ({
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
  }),
});
