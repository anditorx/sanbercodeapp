import React, {useEffect, useState} from 'react';
import {SafeAreaView, StatusBar, StyleSheet, Text, View} from 'react-native';
import {WebView} from 'react-native-webview';
import {Header, Loading} from '../../components';
import {colors} from '../../utils';

const Payment = ({navigation, route}) => {
  const [loading, setLoading] = useState(true);
  const dataParams = route.params;
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, [2000]);
  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
        <Header
          title="Payment"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        <WebView
          style={{flex: 1}}
          source={{uri: dataParams.midtrans.redirect_url}}
        />
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Payment;

const styles = StyleSheet.create({});
