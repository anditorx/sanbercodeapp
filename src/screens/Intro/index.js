import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

import {ImgSlide01, ImgSlide02, ImgSlide03} from '../../assets';
import {colors} from '../../utils';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Button} from '../../components';

const slides = [
  {
    key: 'one',
    title: 'MAKE YOUR IDEAS HAPPEN.',
    text:
      "We're on hand to offer advice on how to best champion your cause and even give you tips on  fundraising, should you need us.",
    image: require('../../assets/images/slide-01.png'),
    backgroundColor: colors.primary,
  },
  {
    key: 'two',
    title: 'LET THE PEOPLE HELP YOU',
    text:
      "We're on hand to offer advice on how to best champion your cause and even give you tips on  fundraising, should you need us.",
    image: require('../../assets/images/slide-02.png'),
    backgroundColor: colors.primary,
  },
  {
    key: 'three',
    title: 'START CREATING YOUR DREAM',
    text:
      "We're on hand to offer advice on how to best champion your cause and even give you tips on  fundraising, should you need us.",
    image: require('../../assets/images/slide-03.png'),
    backgroundColor: colors.primary,
  },
];

const Intro = ({navigation}) => {
  const [showRealApp, setShowRealApp] = useState(false);

  const onDone = () => {
    setShowRealApp(true);
  };
  const onSkip = () => {
    setShowRealApp(true);
  };

  const RenderItem = ({item}) => {
    return (
      <>
        <StatusBar backgroundColor={colors.primary} barStyle="dark-content" />
        <View
          style={{
            flex: 1,
            backgroundColor: item.backgroundColor,
            alignItems: 'center',
            justifyContent: 'center',
            paddingTop: 50,
            paddingBottom: 80,
          }}>
          <Image style={styles.introImageStyle} source={item.image} />
          <Text style={styles.introTitleStyle}>{item.title}</Text>
          <Text style={styles.introTextStyle}>{item.text}</Text>
        </View>
      </>
    );
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="dark-content" />

      <SafeAreaView style={styles.container}>
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          renderNextButton={() => null}
          renderDoneButton={() => null}
        />
        <View style={styles.wrapperBtn}>
          <Button
            title="Sign In"
            type="btn-login-intro"
            onPress={() => navigation.navigate('Login')}
          />
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.txtSignup}>Create Account</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    padding: 10,
  },
  titleStyle: {
    padding: 10,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.white,
  },
  paragraphStyle: {
    padding: 20,
    textAlign: 'center',
    fontSize: 16,
  },
  introImageStyle: {
    width: 250,
    height: 250,
    resizeMode: 'stretch',
  },
  introTextStyle: {
    fontSize: 18,
    color: colors.white,
    textAlign: 'left',
    paddingHorizontal: 20,
  },
  introTitleStyle: {
    fontSize: 30,
    color: colors.black,
    textAlign: 'left',
    fontWeight: 'bold',
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  wrapperBtn: {
    paddingHorizontal: 20,
  },
  txtSignup: {
    textAlign: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },
});
