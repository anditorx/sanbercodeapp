import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Url from '../../api/url';
import {Header, List, Loading} from '../../components';
import {colors} from '../../utils';

const Riwayat = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [token, setToken] = useState(null);
  const [loginWith, setLoginWith] = useState('');
  const [dataTransaksi, setDataTransaksi] = useState('');

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
      if (getAsyncLogin === 'restAPI') {
        getRiwayatDonasi(getAsyncToken);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getRiwayatDonasi = async (getAsyncToken) => {
    setLoading(true);
    await axios
      .get(`${Url.Api}/donasi/riwayat-transaksi`, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + getAsyncToken,
        },
      })
      .then((res) => {
        setLoading(false);
        setDataTransaksi(res.data.data.riwayat_transaksi);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header
          title="Riwayat"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        <FlatList
          data={dataTransaksi}
          renderItem={(item) => (
            <List
              type="list-riwayat"
              text1={item.item.amount.toString()}
              text2={`Donation ID : ${item.item.order_id}`}
              text3={`Tanggal Transaksi : ${item.item.created_at}`}
            />
          )}
          keyExtractor={(item) => item.id}
          // extraData={selectedId}
        />
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Riwayat;

const styles = StyleSheet.create({});
