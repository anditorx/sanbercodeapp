import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';
import {
  ImgBgHeaderHome,
  ImgNoAva,
  IconLikeWhite,
  ImgAva,
  IconTopUp,
  IconRiwayat,
  IconMore,
  IconSaldo,
  IconCircleBantu,
  IconCircleDonasi,
  IconCircleRiwayat,
  IconCircleStatistik,
  ImgHeroSlider01,
} from '../../assets';
import {Gap, Header, Input, Loading, Slider} from '../../components';
import {colors} from '../../utils';
import {SliderBox} from 'react-native-image-slider-box';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Url from '../../api/url';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Home = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [userInfo, setUserInfo] = useState(null);
  const [token, setToken] = useState(null);
  const [photo, setPhoto] = useState('');
  const [loginWith, setLoginWith] = useState('');
  const [img, setImg] = useState([
    'https://source.unsplash.com/1024x768/?nature',
    'https://source.unsplash.com/1024x768/?water',
    'https://source.unsplash.com/1024x768/?girl',
    'https://source.unsplash.com/1024x768/?tree', // Network image
    // require('./assets/images/girl.jpg'), // Local image
  ]);

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
      if (getAsyncLogin === 'restAPI') {
        return getUserFromAPI(getAsyncToken);
      } else if (getAsyncLogin === 'FINGERPRINT') {
        return getUserFromFingerprint();
      } else {
        return getUserFromGoogle();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getUserFromFingerprint = () => {
    const createUser = {
      name: 'Your Name',
      photo: ImgNoAva,
    };
    setUserInfo(createUser);
  };

  const getUserFromGoogle = async () => {
    setLoading(true);
    const userInfo = await GoogleSignin.signInSilently();
    setLoading(false);
    setUserInfo(userInfo.user);
    setPhoto(userInfo.user.photo);
  };

  const getUserFromAPI = async (getAsyncToken) => {
    setLoading(true);
    await axios
      .get(`${Url.Api}/profile/get-profile`, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + getAsyncToken,
        },
      })
      .then((res) => {
        setLoading(false);
        setUserInfo(res.data.data.profile);
        if (res.data.data.profile.photo === null) {
          setPhoto('');
        } else {
          setPhoto(`${Url.Home}${res.data.data.profile.photo}`);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{backgroundColor: colors.white}}>
          <View style={styles.header}>
            {console.log('photo: ', photo)}
            {photo !== '' ? (
              <Header
                type="header-home"
                img={{
                  uri: photo + '?' + new Date(),
                  cache: 'reload',
                  headers: {Pragma: 'no-cache'},
                }}
              />
            ) : (
              <Header type="header-home" img={ImgNoAva} />
            )}
          </View>
          {/* card saldo */}
          <View
            style={{
              backgroundColor: 'transparent',
              marginTop: -70,
              padding: 20,
            }}>
            <View
              style={{
                backgroundColor: colors.white,
                height: 100,
                width: windowWidth - 40,
                borderRadius: 10,
                elevation: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: colors.primary,
                    borderRadius: 10,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      paddingVertical: 10,
                      paddingHorizontal: 10,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                      }}>
                      <Image
                        source={IconSaldo}
                        style={{height: 15, width: 15}}
                      />
                      <Text style={{fontWeight: 'bold', marginLeft: 5}}>
                        Saldo
                      </Text>
                    </View>
                    <Text style={{justifyContent: 'flex-end'}}>
                      Rp15.000.000
                    </Text>
                  </View>
                </View>
              </View>
              <TouchableOpacity
                style={{justifyContent: 'center', alignItems: 'center'}}>
                <Image source={IconTopUp} style={{height: 20, width: 20}} />
                <Text style={{fontWeight: 'bold', fontSize: 13}}>Top Up</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{justifyContent: 'center', alignItems: 'center'}}>
                <Image source={IconRiwayat} style={{height: 20, width: 20}} />
                <Text style={{fontWeight: 'bold', fontSize: 13}}>Riwayat</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{justifyContent: 'center', alignItems: 'center'}}>
                <Image source={IconMore} style={{height: 20, width: 20}} />
                <Text style={{fontWeight: 'bold', fontSize: 13}}>Lainnya</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* slider */}
          <View>
            <Slider />
          </View>
          {/* menu 2 */}
          <View
            style={{
              flexDirection: 'row',
              padding: 20,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Donasi')}
              style={{justifyContent: 'center', alignSelf: 'center'}}>
              <Image
                source={IconCircleDonasi}
                style={{height: 60, width: 60}}
              />
              <Text style={{textAlign: 'center', fontSize: 17}}>Donasi</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('Statistic')}
              style={{justifyContent: 'center', alignSelf: 'center'}}>
              <Image
                source={IconCircleStatistik}
                style={{height: 60, width: 60}}
              />
              <Text style={{textAlign: 'center', fontSize: 17}}>Statistik</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('Riwayat')}
              style={{justifyContent: 'center', alignSelf: 'center'}}>
              <Image
                source={IconCircleRiwayat}
                style={{height: 60, width: 60}}
              />
              <Text style={{textAlign: 'center', fontSize: 17}}>Riwayat</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('Bantu')}
              style={{justifyContent: 'center', alignSelf: 'center'}}>
              <Image source={IconCircleBantu} style={{height: 60, width: 60}} />
              <Text style={{textAlign: 'center', fontSize: 17}}>Bantu</Text>
            </TouchableOpacity>
          </View>
          {/* separator */}
          <View style={{height: 10, backgroundColor: colors.greyLight}}></View>
          {/* News */}
          <View>
            <Text style={{fontWeight: 'bold', fontSize: 20, padding: 20}}>
              Penggalangan Dana Mendesak
            </Text>
            <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{flexDirection: 'row', paddingHorizontal: 20}}>
              <View style={{width: 200, marginRight: 10}}>
                <Image
                  source={ImgHeroSlider01}
                  style={{height: 150, width: 200, borderRadius: 10}}
                />
                <Text
                  style={{
                    marginTop: 10,
                    flexWrap: 'wrap',
                    fontSize: 18,
                  }}>
                  Lorem ipsum dolor sit amet nilson webede niciga
                </Text>
              </View>
              <View style={{width: 200, marginRight: 10}}>
                <Image
                  source={ImgHeroSlider01}
                  style={{height: 150, width: 200, borderRadius: 10}}
                />
                <Text
                  style={{
                    marginTop: 10,
                    flexWrap: 'wrap',
                    fontSize: 18,
                  }}>
                  Lorem ipsum dolor sit amet nilson webede niciga
                </Text>
              </View>
              <View style={{width: 200, marginRight: 10}}>
                <Image
                  source={ImgHeroSlider01}
                  style={{height: 150, width: 200, borderRadius: 10}}
                />
                <Text
                  style={{
                    marginTop: 10,
                    flexWrap: 'wrap',
                    fontSize: 18,
                  }}>
                  Lorem ipsum dolor sit amet nilson webede niciga
                </Text>
              </View>
              <Gap width={30} />
            </ScrollView>
          </View>
          <Gap height={50} />
        </ScrollView>
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Home;

const styles = StyleSheet.create({
  headerRow: {
    backgroundColor: colors.primary,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  header: {
    height: 150,
    backgroundColor: colors.primary,
  },
  txtHeader: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
  },
});
