import axios from 'axios';
import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Url from '../../api/url';
import {IconKeyColor, IconUserColor, ImgLogoColor} from '../../assets';
import {Button, Gap, Header, Input, Loading} from '../../components';
import {colors, flashMessage, useForm} from '../../utils';

const RegisterUpdatePassword = ({navigation, route}) => {
  const dataScreen = route.params;
  const [loading, setLoading] = useState(false);
  const [secureText, setSecureText] = useState(true);
  const [secureText2, setSecureText2] = useState(true);
  const [form, setForm] = useForm({
    email: dataScreen.email,
    name: dataScreen.name,
    password: '',
    confirmPassword: '',
  });
  const showPassword1 = () => {
    if (secureText) {
      setSecureText(false);
    } else {
      setSecureText(true);
    }
  };
  const showPassword2 = () => {
    if (secureText2) {
      setSecureText2(false);
    } else {
      setSecureText2(true);
    }
  };

  const handlingUpdatePassword = async () => {
    setLoading(true);
    if (form.password === '' || form.confirmPassword === '') {
      setLoading(false);
      flashMessage('Failed', `Please fill all form input`, 'danger');
    } else {
      setLoading(true);
      await axios
        .post(`${Url.Api}/auth/update-password`, {
          email: form.email,
          password: form.password,
          password_confirmation: form.confirmPassword,
        })
        .then((res) => {
          setLoading(false);
          if (res.data.response_code == '00') {
            flashMessage(
              'Success',
              `Success sign up. Please sign in to your account`,
              'info',
            );
            navigation.replace('Login');
          } else {
            flashMessage('Failed', `${res.data.response_message}`, 'danger');
          }
        })
        .catch((err) => {
          setLoading(false);
          console.log(JSON.stringify(err));
        });
    }
  };

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
        <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: colors.white,
            flex: 1,
            paddingHorizontal: 20,
          }}>
          <Gap height={35} />
          <Image
            source={ImgLogoColor}
            style={{height: 100, width: 100, alignSelf: 'center'}}
          />
          <Gap height={35} />
          <Text
            style={{
              fontSize: 35,
              color: colors.black,
              fontWeight: 'bold',
              flexWrap: 'wrap',
            }}>
            {`Hi ${dataScreen.name}!`}
          </Text>
          <Text style={{fontSize: 14, color: colors.black, flexWrap: 'wrap'}}>
            Set a new password, for secure your account
          </Text>
          <Gap height={25} />
          <Input
            type="input-icon-password"
            label={'New Password'}
            secureTextEntry={secureText}
            icon={IconKeyColor}
            placeholder={'Enter your password'}
            onPress={showPassword1}
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
          />
          <Gap height={5} />
          <Input
            type="input-icon-password"
            label={'Confirm Password'}
            icon={IconKeyColor}
            secureTextEntry={secureText2}
            onPress={showPassword2}
            placeholder={'Confirm your password'}
            value={form.confirmPassword}
            onChangeText={(value) => setForm('confirmPassword', value)}
          />
          <Gap height={20} />
          {form.password === form.confirmPassword &&
          form.password != '' &&
          form.confirmPassword != '' ? (
            <Button title="Save" onPress={() => handlingUpdatePassword()} />
          ) : (
            <Button title="Save" type="not-active" />
          )}
          <Gap height={20} />
        </ScrollView>
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default RegisterUpdatePassword;

const styles = StyleSheet.create({});
