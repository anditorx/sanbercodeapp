import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Url from '../../api/url';
import {
  IconKeyColor,
  IconUserColor,
  ImgLogoColor,
  IconEmailColor,
  IconGoogleColor,
  IconFingerpringBlack,
} from '../../assets';
import {Button, Gap, Header, Input, Loading} from '../../components';
import {colors, flashMessage, useForm} from '../../utils';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const configFingerprint = {
  title: 'Authentication Required',
  imageColor: colors.black,
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Register = ({navigation}) => {
  const [loading, setLoading] = useState(false);

  const [form, setForm] = useForm({
    email: '',
    fullname: '',
  });

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '707291033897-d1vqpjt73vldp5dq4uckjac6n3eil72m.apps.googleusercontent.com',
    });
  };

  const saveTokenToAsyncStorage = async (token, loginWith) => {
    try {
      if (token !== null || token !== '') {
        await AsyncStorage.setItem('token', token);
        await AsyncStorage.setItem('loginWith', loginWith);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const signInWithGoogle = async () => {
    try {
      setLoading(true);
      const {idToken} = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      saveTokenToAsyncStorage(idToken, 'GOOGLE');
      setLoading(false);
      flashMessage('Success', `Success Sign In`, 'info');
      navigation.replace('Account');
    } catch (error) {
      console.log('[error] signInWithGoogle : ', error);
    }
  };

  let randomStringForFingerprint = Math.random().toString(36).substring(2);

  const signInWithFingerprint = () => {
    TouchID.authenticate('', configFingerprint)
      .then((success) => {
        saveTokenToAsyncStorage(randomStringForFingerprint, 'FINGERPRINT');
        flashMessage('Failed', `Authentication Success`, 'info');
        navigation.replace('Account');
      })
      .catch((error) => {
        flashMessage('Failed', `Authentication Failed`, 'danger');
      });
  };

  const handlingSignUp = async () => {
    setLoading(true);
    if (form.email === '' || form.fullname === '') {
      setLoading(false);
      flashMessage('Failed', `Please fill all form input`, 'danger');
    } else {
      setLoading(true);
      await axios
        .post(`${Url.Api}/auth/register`, {
          name: form.fullname,
          email: form.email,
        })
        .then((res) => {
          setLoading(false);
          navigation.replace('RegisterVerification', form);
        })
        .catch((err) => {
          setLoading(false);
          flashMessage('Failed', `Email is already registered.`, 'danger');
          console.log(JSON.stringify(err));
        });
    }
  };

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
        <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
        <Header
          type="arrow-back-bg-transparent"
          onPress={() => navigation.replace('Intro')}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: colors.white,
            flex: 1,
            paddingHorizontal: 20,
          }}>
          <Image
            source={ImgLogoColor}
            style={{height: 85, width: 85, alignSelf: 'center'}}
          />
          <Gap height={25} />
          <Text style={{fontSize: 27, color: colors.black, fontWeight: 'bold'}}>
            Welcome to Crowdfunding!
          </Text>
          <Text style={{fontSize: 16, color: colors.greyLight}}>
            Please sign up.
          </Text>
          <Gap height={25} />
          <Input
            type={'input-icon'}
            label={'Email'}
            icon={IconEmailColor}
            placeholder={'Enter your email'}
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={5} />
          <Input
            type={'input-icon'}
            label={'Full Name'}
            icon={IconUserColor}
            placeholder={'Enter your name'}
            value={form.fullname}
            onChangeText={(value) => setForm('fullname', value)}
          />
          <Gap height={20} />
          {form.email !== '' && form.fullname !== '' ? (
            <Button title="Sign Up" onPress={handlingSignUp} />
          ) : (
            <Button title="Sign Up" type="not-active" />
          )}
          <Gap height={10} />
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={{textAlign: 'center'}}>Have an account? </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  color: colors.primary,
                }}>
                Sign In
              </Text>
            </TouchableOpacity>
          </View>
          <Gap height={10} />
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={{textAlign: 'center'}}>-- OR -- </Text>
          </View>
          <Gap height={10} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => signInWithGoogle()}
              style={{
                borderColor: colors.greyLight,
                borderWidth: 1,
                width: 100,
                borderRadius: 10,
                padding: 20,
              }}>
              <Image
                source={IconGoogleColor}
                style={{height: 25, width: 25, alignSelf: 'center'}}
              />
            </TouchableOpacity>
            <Gap width={10} />
            <TouchableOpacity
              onPress={() => signInWithFingerprint()}
              style={{
                borderColor: colors.greyLight,
                borderWidth: 1,
                width: 100,
                borderRadius: 10,
                padding: 20,
              }}>
              <Image
                source={IconFingerpringBlack}
                style={{height: 25, width: 25, alignSelf: 'center'}}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Register;

const styles = StyleSheet.create({});
