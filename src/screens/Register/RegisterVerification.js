import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ImgLogoColor} from '../../assets';
import {Button, Gap, Header, Loading} from '../../components';
import {colors, flashMessage} from '../../utils';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import axios from 'axios';
import Url from '../../api/url';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const RegisterVerification = ({navigation, route}) => {
  const dataScreen = route.params;
  const [stateCode, setStateCode] = useState(null);
  const [btnActive, setBtnActive] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState({
    name: dataScreen.fullname,
    email: dataScreen.email,
  });

  const handlingVerify = async () => {
    // setLoading(true);
    if (stateCode == null) {
      setLoading(false);
      flashMessage('Failed', `Please enter the verification code`, 'danger');
    } else {
      setLoading(true);
      await axios
        .post(`${Url.Api}/auth/verification`, {
          otp: stateCode,
        })
        .then((res) => {
          setLoading(false);
          if (res.data.response_code == '01') {
            flashMessage('Failed', `${res.data.response_message}`, 'danger');
          } else {
            flashMessage(
              'Success',
              `Set a new password, for secure your account`,
              'info',
            );
            navigation.replace('RegisterUpdatePassword', data);
          }
        })
        .catch((err) => {
          setLoading(false);

          console.log(JSON.stringify(err));
        });
    }
  };

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
        <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
        <Header
          type="arrow-back-bg-transparent"
          onPress={() => navigation.goBack()}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: colors.white,
            flex: 1,
            paddingHorizontal: 20,
          }}>
          <Image
            source={ImgLogoColor}
            style={{height: 50, width: 50, alignSelf: 'center'}}
          />
          <Gap height={25} />
          <Text style={{fontSize: 30, color: colors.black, fontWeight: 'bold'}}>
            {`Hi ${dataScreen.fullname}!`}
          </Text>
          <Text style={{fontSize: 14, color: colors.black}}>
            Please check your email. We sent 6 digit code to
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: colors.black,
              fontWeight: 'bold',
              flexWrap: 'wrap',
            }}>
            {`${dataScreen.email}`}
          </Text>
          <Gap height={25} />
          {/* <OTPInputView pinCount={6} /> */}
          <OTPInputView
            style={{width: '100%', height: 200}}
            pinCount={6}
            // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={(code) => {
              setStateCode(code);
            }}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={() => setBtnActive(true)}
          />
          {!btnActive ? (
            <Button title="Verify" type="not-active" />
          ) : (
            <Button title="Verify" onPress={() => handlingVerify()} />
          )}
          <Gap height={20} />
          <Text
            style={{
              fontSize: 14,
              color: colors.greyLight,
              textAlign: 'center',
            }}>
            Haven't received the verification code?
          </Text>
          <Gap height={10} />
          <TouchableOpacity>
            <Text
              style={{
                fontSize: 16,
                color: colors.primary,
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              Resend
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default RegisterVerification;

const styles = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: colors.primary,
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 2,
  },

  underlineStyleHighLighted: {
    borderColor: colors.primary,
  },
});
