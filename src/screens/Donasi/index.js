import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ImgHeroSlider02, ImgNoImgAvailable} from '../../assets';
import {Gap, Header, Loading} from '../../components';
import {colors} from '../../utils';
import ProgressBar from 'react-native-progress/Bar';
import axios from 'axios';
import Url from '../../api/url';
import AsyncStorage from '@react-native-async-storage/async-storage';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const Donasi = ({navigation}) => {
  const [dataDonasi, setDataDonasi] = useState(null);
  const [loading, setLoading] = useState(false);
  const [token, setToken] = useState(null);
  const [loginWith, setLoginWith] = useState('');

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
      if (getAsyncLogin === 'restAPI') {
        return getDonasi(getAsyncToken);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDonasi = async (getAsyncToken) => {
    setLoading(true);
    console.log(Url.Api);
    await axios
      .get(`${Url.Api}/donasi/daftar-donasi`, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + getAsyncToken,
        },
      })
      .then((res) => {
        setLoading(false);
        console.log(res.data.data.donasi);
        setDataDonasi(res.data.data.donasi);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const renderDonasi = ({item, index}) => {
    return (
      <>
        <TouchableOpacity
          style={{flexDirection: 'row', paddingHorizontal: 20, marginTop: 10}}
          key={parseInt(index)}
          onPress={() => navigation.navigate('DonasiDetail', item)}>
          <View>
            {item.photo !== null ? (
              <Image
                source={{uri: `${Url.Home}/${item.photo}`}}
                style={{
                  height: windowWidth / 2 - 50,
                  width: windowWidth / 2 - 20,
                  borderRadius: 10,
                }}
              />
            ) : (
              <Image
                source={ImgNoImgAvailable}
                style={{
                  height: windowWidth / 2 - 50,
                  width: windowWidth / 2 - 20,
                  borderRadius: 10,
                }}
              />
            )}
          </View>
          <View
            style={{
              paddingLeft: 10,
              width: windowWidth / 2 - 20,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 18,
                flexWrap: 'wrap',
              }}>
              {item.title}
            </Text>
            {item.user !== null ? (
              <Text>{item.user.name}</Text>
            ) : (
              // console.log(item.user)
              <Text>Noname</Text>
            )}

            <ProgressBar progress={0.3} width={windowWidth / 2 - 30} />
            <Text>Dana yang dibutuhkan</Text>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 18,
                flexWrap: 'wrap',
              }}>
              {`Rp${item.donation}`}
            </Text>
          </View>
        </TouchableOpacity>
        <Gap height={10} />
      </>
    );
  };

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
        <Header
          title="Donasi"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        {dataDonasi !== null ? (
          // dataDonasi.map((item, index) => renderDonasi(item, index))
          <FlatList
            data={dataDonasi}
            renderItem={renderDonasi}
            keyExtractor={(item) => item.id.toString()}
          />
        ) : (
          <View></View>
        )}
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Donasi;

const styles = StyleSheet.create({});
