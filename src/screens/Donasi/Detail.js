import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import NumberFormat from 'react-number-format';
import Url from '../../api/url';
import {IconBackColor, ImgNoAva} from '../../assets';
import {Button, Gap, Header, Input, Loading, Separator} from '../../components';
import {colors, useForm} from '../../utils';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  SelectMultipleButton,
  SelectMultipleGroupButton,
} from 'react-native-selectmultiple-button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Detail = ({navigation, route}) => {
  const dataScreen = route.params;
  const refRBSheet = useRef();
  const [bgBtn, setBgBtn] = useState({
    btn1: colors.white,
    btn2: colors.white,
    btn3: colors.white,
    btn4: colors.white,
    btn5: colors.white,
    btn6: colors.white,
  });
  const [loading, setLoading] = useState(false);
  const [nominal, setNominal] = useState(0);
  const [check, setCheck] = useState(false);
  const [userInfo, setUserInfo] = useState(null);
  const [loginWith, setLoginWith] = useState('');
  const [token, setToken] = useState(null);

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
      if (getAsyncLogin === 'restAPI') {
        return getUserFromAPI(getAsyncToken);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const getUserFromAPI = async (getAsyncToken) => {
    setLoading(true);
    await axios
      .get(`${Url.Api}/profile/get-profile`, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + getAsyncToken,
        },
      })
      .then((res) => {
        setLoading(false);
        setUserInfo(res.data.data.profile);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const chooseNominal = (val) => {
    check === true ? setCheck(false) : setCheck(true);
    setNominal(val);
    switch (val) {
      case 10000:
        setNominal(val);
        setBgBtn({
          btn1: colors.primary,
          btn2: colors.white,
          btn3: colors.white,
          btn4: colors.white,
          btn5: colors.white,
          btn6: colors.white,
        });
        break;
      case 20000:
        setNominal(val);
        setBgBtn({
          btn1: colors.white,
          btn2: colors.primary,
          btn3: colors.white,
          btn4: colors.white,
          btn5: colors.white,
          btn6: colors.white,
        });
        break;
      case 50000:
        setNominal(val);
        setBgBtn({
          btn1: colors.white,
          btn2: colors.white,
          btn3: colors.primary,
          btn4: colors.white,
          btn5: colors.white,
          btn6: colors.white,
        });
        break;
      case 100000:
        setNominal(val);
        setBgBtn({
          btn1: colors.white,
          btn2: colors.white,
          btn3: colors.white,
          btn4: colors.primary,
          btn5: colors.white,
          btn6: colors.white,
        });
        break;
      case 200000:
        setNominal(val);
        setBgBtn({
          btn1: colors.white,
          btn2: colors.white,
          btn3: colors.white,
          btn4: colors.white,
          btn5: colors.primary,
          btn6: colors.white,
        });
        break;
      case 500000:
        setNominal(val);
        setBgBtn({
          btn1: colors.white,
          btn2: colors.white,
          btn3: colors.white,
          btn4: colors.white,
          btn5: colors.white,
          btn6: colors.primary,
        });
        break;
      default:
        break;
    }
  };

  const onPayPress = async () => {
    setLoading(true);
    let random = Math.random().toString(36).substring(2);
    console.log('USER INFO : ', userInfo);
    console.log('token : ', token);
    console.log('dataScreen :', dataScreen);
    console.log('nominal :', nominal);
    console.log('random string :', 'order-' + random);
    const body = {
      transaction_details: {
        order_id: `donasi-${random}`,
        gross_amount: parseInt(nominal),
        donation_id: dataScreen.id,
      },
      customer_details: {
        first_name: userInfo.name,
        last_name: '',
        email: userInfo.email,
        phone: '',
      },
    };
    await axios
      .post(`${Url.Api}/donasi/generate-midtrans`, body, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + token,
        },
      })
      .then((res) => {
        setLoading(false);
        console.log(res);
        if (res) {
          const data = res.data.data;
          navigation.replace('Payment', data);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{backgroundColor: colors.white, flex: 1}}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              position: 'absolute',
              zIndex: 1,
              backgroundColor: colors.white,
              height: 30,
              width: 30,
              borderRadius: 30 / 2,
              justifyContent: 'center',
              alignItems: 'center',
              marginHorizontal: 20,
              marginTop: 20,
            }}>
            <Image source={IconBackColor} style={{height: 20, width: 20}} />
          </TouchableOpacity>
          <Image
            source={{
              uri: `${Url.Home}/${dataScreen.photo}` + '?' + new Date(),
              cache: 'reload',
              headers: {Pragma: 'no-cache'},
            }}
            style={{width: windowWidth, height: 250}}
          />
          <View style={{paddingHorizontal: 20, paddingTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 23}}>
              {dataScreen.title}
            </Text>
            <Text style={{fontSize: 14}}>
              Dana yang dibutuhkan Rp{dataScreen.donation}
            </Text>
            <Text style={{fontWeight: 'bold', fontSize: 17, paddingTop: 10}}>
              Deskripsi
            </Text>
            <Text style={{fontSize: 16}}>{dataScreen.description}</Text>
            <Gap height={25} />
            <Button
              title="Donasi Sekarang"
              onPress={() => refRBSheet.current.open()}
            />
            <Gap height={25} />
          </View>
          <Separator />
          <View style={{paddingHorizontal: 20, paddingTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 17}}>
              Informasi Penggalangan Dana
            </Text>
            <Gap height={10} />
            <View
              style={{
                paddingTop: 10,
                height: 120,
                width: '100%',
                backgroundColor: colors.white,
                borderRadius: 10,
                elevation: 10,
              }}>
              <View style={{paddingHorizontal: 20}}>
                <Text>Penggalang Dana</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingHorizontal: 20,
                  paddingTop: 20,
                  alignItems: 'center',
                }}>
                <Image
                  source={ImgNoAva}
                  style={{
                    height: 50,
                    width: 50,
                    borderRadius: 50 / 2,
                    zIndex: 1,
                  }}
                />
                <Gap width={10} />
                <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                  {dataScreen.user.name}
                </Text>
              </View>
            </View>
          </View>
          <Gap height={50} />
        </ScrollView>
        {/* RBSHEET */}
        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={420}
          openDuration={350}
          customStyles={{
            wrapper: {
              // backgroundColor: 'transparent',
              // justifyContent: 'center',
              // alignItems: 'center',
            },
            draggableIcon: {
              backgroundColor: colors.primary,
            },
            container: {
              backgroundColor: colors.white,
              borderTopRightRadius: 40,
              borderTopLeftRadius: 40,
              elevation: 40,
            },
          }}>
          <View style={{padding: 20}}>
            <Text>Isi Nominal</Text>
            {/* <Text>Rp. {nominal}</Text> */}
            <NumberFormat
              value={nominal.toString()}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp'}
              renderText={(value) => <Text>{value}</Text>}
            />
            <Input
              type="number"
              value={nominal.toString()}
              onChangeText={(value) => setNominal(value)}
            />
            <Gap height={30} />
            <View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Button
                  onPress={() => chooseNominal(10000)}
                  type="btn-label-donation-1"
                  title="Rp.10.000"
                  check={check}
                  bgCol={bgBtn.btn1}
                />
                <Button
                  onPress={() => chooseNominal(20000)}
                  type="btn-label-donation-2"
                  title="Rp.20.000"
                  bgCol={bgBtn.btn2}
                />
                <Button
                  onPress={() => chooseNominal(50000)}
                  type="btn-label-donation-3"
                  title="Rp.50.000"
                  bgCol={bgBtn.btn3}
                />
              </View>
              <Gap height={20} />
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Button
                  onPress={() => chooseNominal(100000)}
                  type="btn-label-donation-4"
                  title="Rp.100.000"
                  bgCol={bgBtn.btn4}
                />
                <Button
                  onPress={() => chooseNominal(200000)}
                  type="btn-label-donation-5"
                  title="Rp.200.000"
                  bgCol={bgBtn.btn5}
                />
                <Button
                  onPress={() => chooseNominal(500000)}
                  type="btn-label-donation-6"
                  title="Rp.500.000"
                  bgCol={bgBtn.btn6}
                />
              </View>
            </View>
            <Gap height={35} />
            {nominal !== 0 ? (
              <Button title="Lanjut" onPress={() => onPayPress()} />
            ) : (
              <Button title="Lanjut" type="not-active" />
            )}
          </View>
        </RBSheet>
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Detail;

const styles = StyleSheet.create({});
