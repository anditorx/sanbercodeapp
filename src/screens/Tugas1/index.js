import React from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';

const Tugas1 = () => {
  return (
    <>
      <SafeAreaView style={styles.page}>
        <View>
          <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Tugas1;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
