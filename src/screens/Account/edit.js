import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import Url from '../../api/url';
import {
  IconCameraWhite,
  ImgAva,
  ImgNoAva,
  IconCameraFlipBlack,
  IconPressPhoto,
} from '../../assets';
import {Gap, Header, Input, Loading, UploadPhoto} from '../../components';
import {colors, flashMessage, useForm} from '../../utils';
import {RNCamera} from 'react-native-camera';
import axios from 'axios';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const edit = ({navigation, route}) => {
  let camera = useRef(null);
  let input = useRef(null);

  const dataScreen = route.params;
  console.log('datascreen :', dataScreen);
  const [userInfo, setUserInfo] = useState(dataScreen);
  const [form, setForm] = useForm({
    name: dataScreen.name,
    photo: dataScreen.photo,
  });
  console.log(dataScreen.photo);
  const [token, setToken] = useState(null);
  const [loginWith, setLoginWith] = useState('');
  const [loading, setLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  // const [photo, setPhoto] = useState(`${Url.Home}${dataScreen.photo}`);
  const [photo, setPhoto] = useState(dataScreen.photo);
  const [type, setType] = useState('back');
  const [takePhoto, setTakePhoto] = useState(null);

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
    } catch (error) {
      console.log(error);
    }
  };

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setTakePhoto(data);
      setPhoto(data.uri);
      setForm('photo', data.uri);
      setIsVisible(false);
    }
  };

  const handlingSave = async () => {
    setLoading(true);
    const formGroup = new FormData();
    formGroup.append('name', form.name);
    formGroup.append('photo', {
      uri: form.photo,
      name: 'photo.jpg',
      type: 'image/jpg',
    });
    await axios
      .post(`${Url.Api}/profile/update-profile`, formGroup, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + token,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((res) => {
        setLoading(false);
        flashMessage('Success', `${res.data.response_message}`, 'info');
        navigation.replace('Account');
      })
      .catch((err) => {
        setLoading(false);
      });
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1, justifyContent: 'space-between'}}
            type={type}
            ref={camera}>
            <View style={{padding: 20}}>
              <TouchableOpacity
                onPress={() => toogleCamera()}
                style={{
                  backgroundColor: colors.white,
                  height: 35,
                  width: 35,
                  borderRadius: 35 / 2,
                  padding: 5,
                }}>
                <Image
                  source={IconCameraFlipBlack}
                  style={{height: 25, width: 25}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: 250,
                width: 200,
                // backgroundColor: 'yellow',
                borderWidth: 1,
                borderColor: colors.white,
                borderRadius: 100,
                justifyContent: 'center',
                alignSelf: 'center',
              }}></View>
            <Gap height={10} />
            <View
              style={{
                height: 150,
                width: 250,
                // backgroundColor: 'yellow',
                borderWidth: 1,
                borderColor: colors.white,
                borderRadius: 20,
                justifyContent: 'center',
                alignSelf: 'center',
              }}></View>
            <View
              style={{
                padding: 20,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={() => takePicture()}>
                <Image
                  source={IconPressPhoto}
                  style={{height: 60, width: 60}}
                />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header
          title="Edit Account"
          type="btn-back-and-save"
          onPressBack={() => navigation.goBack()}
          onPressSave={() => handlingSave()}
        />
        <View style={styles.body}>
          <View style={styles.boxFotoAndName}>
            {photo === null ? (
              <UploadPhoto
                type={'add-photo'}
                img={ImgNoAva}
                onPress={() => setIsVisible(true)}
              />
            ) : (
              <UploadPhoto
                type={'add-photo'}
                img={{
                  uri: Url.Home + photo + '?' + new Date(),
                  cache: 'reload',
                  headers: {Pragma: 'no-cache'},
                }}
                onPress={() => setIsVisible(true)}
              />
            )}
          </View>
          <Gap height={30} />
          <View style={styles.form}>
            <Input
              label="Full Name"
              value={form.name}
              onChangeText={(value) => setForm('name', value)}
            />
            <Gap height={10} />
            <Input label="Email" value={'email'} disable />
          </View>
        </View>
        {renderCamera()}
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default edit;

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
  },
  boxFotoAndName: {
    height: 100,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    paddingTop: 150,
    alignItems: 'center',
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 100 / 2,
  },
  form: {
    paddingHorizontal: 20,
    width: '100%',
  },
});
