import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  IconHelp,
  IconLogout,
  IconSetting,
  IconTerms,
  IconWallet,
  ImgNoAva,
} from '../../assets';
import {Header, Loading} from '../../components';
import {colors, configureGoogleSignIn, flashMessage} from '../../utils';
import axios from 'axios';
import Url from '../../api/url';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';

const Account = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [userInfo, setUserInfo] = useState(null);
  const [photo, setPhoto] = useState('');
  const [token, setToken] = useState(null);
  const [loginWith, setLoginWith] = useState('');

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
      if (getAsyncLogin === 'restAPI') {
        return getUserFromAPI(getAsyncToken);
      } else if (getAsyncLogin === 'FINGERPRINT') {
        return getUserFromFingerprint();
      } else {
        return getUserFromGoogle();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getUserFromFingerprint = () => {
    const createUser = {
      name: 'Your Name',
      photo: ImgNoAva,
    };
    setUserInfo(createUser);
  };

  const getUserFromGoogle = async () => {
    setLoading(true);
    const userInfo = await GoogleSignin.signInSilently();
    setLoading(false);
    setUserInfo(userInfo.user);
    setPhoto(userInfo.user.photo);
  };

  const getUserFromAPI = async (getAsyncToken) => {
    setLoading(true);
    await axios
      .get(`${Url.Api}/profile/get-profile`, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + getAsyncToken,
        },
      })
      .then((res) => {
        setLoading(false);
        setUserInfo(res.data.data.profile);
        if (res.data.data.profile.photo === null) {
          setPhoto('');
        } else {
          setPhoto(`${Url.Home}${res.data.data.profile.photo}`);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const onPressLogOut = async () => {
    setLoading(true);
    try {
      await GoogleSignin.signOut();
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('loginWith');
      setLoading(false);
      flashMessage('Success', `Success Logout`, 'info');
      // navigation.replace('Login');
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const alertLogout = () => {
    Alert.alert(
      `Keluar Aplikasi`,
      `Anda yakin ingin keluar aplikasi?`,
      [
        {text: 'OK', onPress: onPressLogOut},
        {
          text: 'Tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        {loginWith === 'restAPI' ? (
          <Header
            title="Account"
            type="btn-back-and-edit"
            onPressBack={() => navigation.goBack()}
            onPress={() => navigation.navigate('AccountEdit', userInfo)}
          />
        ) : (
          <Header title="Account" type="btn-back" />
        )}
        {userInfo !== null && (
          <View style={styles.body}>
            <View style={styles.boxFotoAndName}>
              {photo !== '' ? (
                <Image
                  source={{
                    uri: photo + '?' + new Date(),
                    cache: 'reload',
                    headers: {Pragma: 'no-cache'},
                  }}
                  style={styles.avatar}
                />
              ) : (
                <Image source={ImgNoAva} style={styles.avatar} />
              )}
              <Text style={styles.fullname}>{userInfo.name}</Text>
            </View>
            <View style={styles.boxSaldo}>
              <View style={styles.wrapperIcon}>
                <Image source={IconWallet} style={styles.icon} />
                <Text style={styles.labelIcon}>Saldo</Text>
              </View>
              <Text style={styles.saldo}>Rp.55.000</Text>
            </View>
            <View style={styles.box}>
              <View style={styles.wrapperIcon}>
                <Image source={IconSetting} style={styles.icon} />
                <Text style={styles.labelIcon}>Pengaturan</Text>
              </View>
            </View>
            <TouchableOpacity
              style={styles.box}
              onPress={() => navigation.navigate('Bantuan')}>
              <View style={styles.wrapperIcon}>
                <Image source={IconHelp} style={styles.icon} />
                <Text style={styles.labelIcon}>Bantuan</Text>
              </View>
            </TouchableOpacity>
            <View style={styles.box}>
              <View style={styles.wrapperIcon}>
                <Image source={IconTerms} style={styles.icon} />
                <Text style={styles.labelIcon}>Syarat & Ketentuan</Text>
              </View>
            </View>
            <TouchableOpacity style={styles.boxLogout} onPress={alertLogout}>
              <View style={styles.wrapperIcon}>
                <Image source={IconLogout} style={styles.icon} />
                <Text style={styles.labelIcon}>Keluar</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Account;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#1297E0',
    height: 60,
    justifyContent: 'center',
    padding: 20,
  },
  txtHeader: {
    color: 'white',
    fontSize: 24,
  },
  body: {
    flex: 1,
    backgroundColor: colors.greyLight,
  },
  boxFotoAndName: {
    height: 100,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
  },
  fullname: {
    fontSize: 20,
    marginLeft: 20,
    fontWeight: 'bold',
  },
  boxSaldo: {
    marginTop: 2,
    marginBottom: 5,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  wrapperIcon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    height: 30,
    width: 30,
  },
  labelIcon: {
    marginLeft: 20,
    fontSize: 18,
  },
  saldo: {
    fontSize: 18,
  },
  box: {
    marginTop: 2,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  boxLogout: {
    marginTop: 7,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
