import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  IconFingerpringBlack,
  IconGoogleColor,
  IconKeyColor,
  IconUserColor,
  ImgLogoColor,
} from '../../assets';
import {Button, Gap, Header, Input, Loading, Separator} from '../../components';
import {colors, flashMessage, useForm} from '../../utils';
import axios from 'axios';
import Url from '../../api/url';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';
import configureGoogleSignIn from '../../utils/configureGoogleSignIn';
import TouchID from 'react-native-touch-id';

const configFingerprint = {
  title: 'Authentication Required',
  imageColor: colors.black,
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Login = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const [secureText, setSecureText] = useState(true);

  const [form, setForm] = useForm({
    email: '',
    password: '',
  });

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '707291033897-d1vqpjt73vldp5dq4uckjac6n3eil72m.apps.googleusercontent.com',
    });
  };

  const saveTokenToAsyncStorage = async (token, loginWith) => {
    try {
      if (token !== null || token !== '') {
        await AsyncStorage.setItem('token', token);
        await AsyncStorage.setItem('loginWith', loginWith);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const showPassword = () => {
    if (secureText) {
      setSecureText(false);
    } else {
      setSecureText(true);
    }
  };

  const onPressSignIn = () => {
    if (form.password === '' || form.email === '') {
      flashMessage('Warning', 'Please fill all input form.', 'danger');
    } else {
      setLoading(true);
      axios
        .post(`${Url.Api}/auth/login`, {
          email: form.email,
          password: form.password,
        })
        .then((res) => {
          saveTokenToAsyncStorage(res.data.data.token, 'restAPI');
          flashMessage('Success', `${res.data.response_message}`, 'info');
          setLoading(false);
          // navigation.replace('MainApp');
          navigation.reset({
            index: 0,
            routes: [{name: 'MainApp'}],
          });
        })
        .catch((err) => {
          setLoading(false);
          flashMessage('Gagal', `Email atau password salah.`, 'danger');
          console.log(JSON.stringify(err));
        });
    }
  };

  const signInWithGoogle = async () => {
    try {
      setLoading(true);
      const {idToken} = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      saveTokenToAsyncStorage(idToken, 'GOOGLE');
      setLoading(false);
      flashMessage('Success', `Success Sign In`, 'info');
      // navigation.replace('MainApp');
      navigation.reset({
        index: 0,
        routes: [{name: 'MainApp'}],
      });
    } catch (error) {
      console.log('[error] signInWithGoogle : ', error);
    }
  };

  let randomStringForFingerprint = Math.random().toString(36).substring(2);

  const signInWithFingerprint = () => {
    TouchID.authenticate('', configFingerprint)
      .then((success) => {
        saveTokenToAsyncStorage(randomStringForFingerprint, 'FINGERPRINT');
        flashMessage('Failed', `Authentication Success`, 'info');
        // navigation.replace('MainApp');
        navigation.reset({
          index: 0,
          routes: [{name: 'MainApp'}],
        });
      })
      .catch((error) => {
        flashMessage('Failed', `Authentication Failed`, 'danger');
      });
  };

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
        <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
        <Header
          type="arrow-back-bg-transparent"
          onPress={() => navigation.replace('Intro')}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: colors.white,
            flex: 1,
            paddingTop: 25,
            paddingHorizontal: 20,
          }}>
          <Image
            source={ImgLogoColor}
            style={{height: 85, width: 85, alignSelf: 'center'}}
          />
          <Gap height={50} />
          <Text style={{fontSize: 27, color: colors.black, fontWeight: 'bold'}}>
            Welcome Back!
          </Text>
          <Text style={{fontSize: 16, color: colors.greyLight}}>
            Please sign in to your account.
          </Text>
          <Gap height={25} />
          <Input
            type={'input-icon'}
            label={'Email'}
            icon={IconUserColor}
            placeholder={'Enter your email'}
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={5} />
          <Input
            type={'input-icon-password'}
            label={'Password'}
            icon={IconKeyColor}
            placeholder={'Enter your password'}
            secureTextEntry={secureText}
            onPress={showPassword}
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
          />
          <Gap height={20} />
          {form.email !== '' && form.password !== '' ? (
            <Button title="Sign In" onPress={() => onPressSignIn()} />
          ) : (
            <Button title="Sign In" type="not-active" />
          )}

          <Gap height={10} />

          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={{textAlign: 'center'}}>-- Or Sign In With -- </Text>
          </View>
          <Gap height={10} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => signInWithGoogle()}
              style={{
                borderColor: colors.greyLight,
                borderWidth: 1,
                width: 100,
                borderRadius: 10,
                padding: 20,
              }}>
              <Image
                source={IconGoogleColor}
                style={{height: 25, width: 25, alignSelf: 'center'}}
              />
            </TouchableOpacity>
            <Gap width={10} />
            <TouchableOpacity
              onPress={() => signInWithFingerprint()}
              style={{
                borderColor: colors.greyLight,
                borderWidth: 1,
                width: 100,
                borderRadius: 10,
                padding: 20,
              }}>
              <Image
                source={IconFingerpringBlack}
                style={{height: 25, width: 25, alignSelf: 'center'}}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Login;

const styles = StyleSheet.create({});
