import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {IconCamera2, IconCameraFlipBlack, IconPressPhoto} from '../../assets';
import {Button, Gap, Header, Input, Loading} from '../../components';
import {colors, flashMessage, useForm} from '../../utils';
import {RNCamera} from 'react-native-camera';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Url from '../../api/url';
import {TextInputMask} from 'react-native-masked-text';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Bantu = ({navigation}) => {
  let camera = useRef(null);
  const [token, setToken] = useState(null);
  const [loginWith, setLoginWith] = useState('');
  const [loading, setLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [photo, setPhoto] = useState(``);
  const [type, setType] = useState('back');
  const [takePhoto, setTakePhoto] = useState(null);
  const [form, setForm] = useForm({
    photo: null,
    judul: '',
    deskripsi: '',
    dana: 0,
  });

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
    } catch (error) {
      console.log(error);
    }
  };

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setTakePhoto(data);
      setPhoto(data.uri);
      setForm('photo', data.uri);
      setIsVisible(false);
    }
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1, justifyContent: 'space-between'}}
            type={type}
            ref={camera}>
            <View style={{padding: 20}}>
              <TouchableOpacity
                onPress={() => toogleCamera()}
                style={{
                  backgroundColor: colors.white,
                  height: 35,
                  width: 35,
                  borderRadius: 35 / 2,
                  padding: 5,
                }}>
                <Image
                  source={IconCameraFlipBlack}
                  style={{height: 25, width: 25}}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                padding: 20,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={() => takePicture()}>
                <Image
                  source={IconPressPhoto}
                  style={{height: 60, width: 60}}
                />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  const handlingSave = async () => {
    console.log('Form : ', form);
    console.log('token : ', token);
    setLoading(true);
    const formGroup = new FormData();
    formGroup.append('title', form.judul);
    formGroup.append('description', form.deskripsi);
    formGroup.append('donation', parseInt(form.dana));
    formGroup.append('photo', {
      uri: form.photo,
      name: 'photo.jpg',
      type: 'image/jpg',
    });
    await axios
      .post(`${Url.Api}/donasi/tambah-donasi`, formGroup, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + token,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((res) => {
        setLoading(false);
        flashMessage('Success', `${res.data.response_message}`, 'info');
        navigation.reset({
          index: 0,
          routes: [{name: 'MainApp'}],
        });
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };
  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header
          title="Buat Donasi"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          {form.photo === null ? (
            <TouchableOpacity
              onPress={() => setIsVisible(true)}
              style={{
                backgroundColor: colors.greyLight,
                width: windowWidth,
                height: 200,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={IconCamera2} style={{height: 50, width: 50}} />
              <Text style={{fontWeight: 'bold', fontSize: 18}}>
                Ambil Gambar
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => setIsVisible(true)}
              style={{
                backgroundColor: colors.greyLight,
                width: windowWidth,
                height: 250,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={{
                  uri: photo + '?' + new Date(),
                  cache: 'reload',
                  headers: {Pragma: 'no-cache'},
                }}
                style={{width: windowWidth, height: 250}}
              />
            </TouchableOpacity>
          )}
          <View style={{padding: 20}}>
            <Input
              label="Judul"
              value={form.judul}
              onChangeText={(value) => setForm('judul', value)}
            />
            <Gap height={20} />
            <Input
              label="Deskripsi"
              type="textarea"
              value={form.deskripsi}
              onChangeText={(value) => setForm('deskripsi', value)}
            />
            <Gap height={20} />
            <Input
              label="Dana yang dibutuhkan"
              type="number"
              value={form.dana.toString()}
              onChangeText={(value) => setForm('dana', value)}
            />
            <Gap height={20} />
            {form.photo !== null &&
            form.judul !== '' &&
            form.deskripsi !== '' &&
            form.dana !== 0 ? (
              <Button title="Simpan" onPress={() => handlingSave()} />
            ) : (
              <Button title="Simpan" type="not-active" />
            )}
          </View>
        </ScrollView>
        {renderCamera()}
      </SafeAreaView>
      {loading && <Loading />}
    </>
  );
};

export default Bantu;

const styles = StyleSheet.create({});
