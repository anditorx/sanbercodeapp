import React, {useEffect, useState} from 'react';
import {SafeAreaView, StatusBar, StyleSheet, Text, View} from 'react-native';
import {Header} from '../../components';
import {colors} from '../../utils';
import database from '@react-native-firebase/database';
import {GiftedChat} from 'react-native-gifted-chat';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Url from '../../api/url';

const Inbox = ({navigation}) => {
  const [user, setUser] = useState(null);
  const [messages, setMessages] = useState([]);
  const [token, setToken] = useState(null);
  const [loginWith, setLoginWith] = useState('');
  const [loading, setLoading] = useState(false);
  const [photo, setPhoto] = useState('');

  useEffect(() => {
    getToken();
    onRef();

    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getToken = async () => {
    try {
      const getAsyncToken = await AsyncStorage.getItem('token');
      const getAsyncLogin = await AsyncStorage.getItem('loginWith');
      setToken(getAsyncToken);
      setLoginWith(getAsyncLogin);
      if (getAsyncLogin === 'restAPI') {
        return getUserFromAPI(getAsyncToken);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getUserFromAPI = async (getAsyncToken) => {
    setLoading(true);
    await axios
      .get(`${Url.Api}/profile/get-profile`, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + getAsyncToken,
        },
      })
      .then((res) => {
        setLoading(false);
        console.log(res.data.data.profile);
        setUser(res.data.data.profile);
        if (res.data.data.profile.photo === null) {
          setPhoto('');
        } else {
          setPhoto(`${Url.Home}${res.data.data.profile.photo}`);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const onRef = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };

  const onSend = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header
          title="Inbox"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        <View style={styles.page}>
          {user !== null && (
            <GiftedChat
              messages={messages}
              onSend={(messages) => onSend(messages)}
              user={{
                _id: user.id,
                name: user.name,
                avatar: photo,
              }}
              showUserAvatar
            />
          )}
        </View>
      </SafeAreaView>
    </>
  );
};

export default Inbox;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
});
