import Tugas1 from './Tugas1';
import Tugas2 from './Tugas2';
import Tugas3 from './Tugas3';
import Tugas4 from './Tugas4';
import MiniProjek1 from './MiniProjek1';
import Splashscreen from './Splashscreen';
import Intro from './Intro';
import Login from './Login';
import Account from './Account';
import AccountEdit from './Account/edit';
import Register from './Register';
import RegisterVerification from './Register/RegisterVerification';
import RegisterUpdatePassword from './Register/RegisterUpdatePassword';
import Home from './Home';
import Donasi from './Donasi';
import DonasiDetail from './Donasi/Detail';
import Payment from './Payment';
import Bantuan from './Bantuan';
import Statistic from './Statistic';
import Inbox from './Inbox';
import Riwayat from './Riwayat';
import Bantu from './Bantu';

export {
  Bantu,
  Riwayat,
  Tugas1,
  Tugas2,
  Tugas3,
  Tugas4,
  MiniProjek1,
  Splashscreen,
  Intro,
  Login,
  Account,
  AccountEdit,
  Register,
  RegisterVerification,
  RegisterUpdatePassword,
  Home,
  Donasi,
  DonasiDetail,
  Payment,
  Bantuan,
  Inbox,
  Statistic,
};
