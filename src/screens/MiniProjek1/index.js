import React, {useContext, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {IconTrash} from '../../assets';
import {Button, Gap, Header, Input, List, Separator} from '../../components';
import {MiniProject1Context} from '../../context/Context_MiniProjek1';
import {flashMessage, useForm} from '../../utils';
import {colors} from '../../utils/colors';

const MiniProjek1 = () => {
  const [contact, setContact] = useContext(MiniProject1Context);
  const [form, setForm] = useForm({
    fullName: '',
    phoneNumber: '',
  });

  const onPressSave = () => {
    if (form.fullName !== '' && form.phoneNumber !== '') {
      setContact([
        ...contact,
        {fullName: form.fullName, phoneNumber: form.phoneNumber},
      ]);
      flashMessage('Success', 'Success create contact.', 'info');
      setForm('reset');
    } else {
      flashMessage('Warning', 'Please fill all input form.', 'warning');
    }
  };

  handleDelete = (index) => {
    let newData = contact;
    newData.splice(index, 1);
    setContact([...newData]);
    flashMessage('Success', 'Success delete contact.', 'info');
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header title="Mini Projek 1" />
        <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
          {/* Form Input */}
          <Input
            placeholder="Name"
            value={form.fullName}
            onChangeText={(value) => setForm('fullName', value)}
          />
          <Input
            placeholder="Phone Number"
            type="number"
            value={form.phoneNumber}
            onChangeText={(value) => setForm('phoneNumber', value)}
          />
          <Gap height={25} />
          <Button title="Save" onPress={onPressSave} />
          <Gap height={30} />
          <Separator />
          <Gap height={30} />
          {/* LIST */}
          <View style={styles.wrapperList}>
            {contact !== [] &&
              contact.map((item, index) => (
                <List
                  key={index}
                  text1={item.fullName}
                  text2={item.phoneNumber}
                  onPress={() => handleDelete(index)}
                />
              ))}
          </View>
          <Gap height={50} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default MiniProjek1;

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: colors.body,
    paddingHorizontal: 20,
  },
  input: (border) => ({
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
  }),
  wrapperList: {
    flex: 1,
  },
  btnAdd: {
    height: 50,
    width: 50,
    backgroundColor: '#1297E0',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  wrapperInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapperRowList: {
    borderWidth: 3,
    borderColor: '#CDC7C7',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
});
