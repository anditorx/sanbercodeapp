import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import {
  IconHelp,
  IconLogout,
  IconSetting,
  IconTerms,
  IconWallet,
  ImgAva,
} from '../../assets';

const Tugas2 = () => {
  return (
    <>
      <StatusBar backgroundColor="#1297E0" barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.header}>
          <Text style={styles.txtHeader}>Account</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.boxFotoAndName}>
            <Image source={ImgAva} style={styles.avatar} />
            <Text style={styles.fullname}>Andi Rustianto</Text>
          </View>
          <View style={styles.boxSaldo}>
            <View style={styles.wrapperIcon}>
              <Image source={IconWallet} style={styles.icon} />
              <Text style={styles.labelIcon}>Saldo</Text>
            </View>
            <Text style={styles.saldo}>Rp.55.000</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.wrapperIcon}>
              <Image source={IconSetting} style={styles.icon} />
              <Text style={styles.labelIcon}>Pengaturan</Text>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.wrapperIcon}>
              <Image source={IconHelp} style={styles.icon} />
              <Text style={styles.labelIcon}>Bantuan</Text>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.wrapperIcon}>
              <Image source={IconTerms} style={styles.icon} />
              <Text style={styles.labelIcon}>Syarat & Ketentuan</Text>
            </View>
          </View>
          <View style={styles.boxLogout}>
            <View style={styles.wrapperIcon}>
              <Image source={IconLogout} style={styles.icon} />
              <Text style={styles.labelIcon}>Keluar</Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Tugas2;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#1297E0',
    height: 60,
    justifyContent: 'center',
    padding: 20,
  },
  txtHeader: {
    color: 'white',
    fontSize: 24,
  },
  body: {
    flex: 1,
    backgroundColor: '#dfe4ea',
  },
  boxFotoAndName: {
    height: 100,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
  },
  fullname: {
    fontSize: 20,
    marginLeft: 20,
    fontWeight: 'bold',
  },
  boxSaldo: {
    marginTop: 2,
    marginBottom: 5,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  wrapperIcon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    height: 30,
    width: 30,
  },
  labelIcon: {
    marginLeft: 20,
    fontSize: 18,
  },
  saldo: {
    fontSize: 18,
  },
  box: {
    marginTop: 2,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  boxLogout: {
    marginTop: 7,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
