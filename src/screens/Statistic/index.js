import React, {useState} from 'react';
import {
  processColor,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Chart, Header} from '../../components';
import {colors} from '../../utils';
import {BarChart} from 'react-native-charts-wrapper';

const Statistic = ({navigation}) => {
  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
        <Header
          title="Statistic"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        <Chart />
      </SafeAreaView>
    </>
  );
};

export default Statistic;

const styles = StyleSheet.create({});
