import React, {useContext, useEffect, useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {IconPlus, IconTrash} from '../../assets';
import {Header} from '../../components';
import Tugas4Context from '../../context/Context_Tugas4';

const FormAndList = () => {
  const stateContext = useContext(Tugas4Context);

  const [border, setBorder] = useState('#444447');
  const onFocusForm = () => {
    setBorder('#1297E0');
  };
  const onBlurFrom = () => {
    setBorder('#444447');
  };

  const renderList = ({item, index}) => {
    return (
      <View key={index} style={styles.wrapperRowList}>
        <View>
          <Text style={{fontSize: 12, color: '#AAA6A6'}}>{item.date}</Text>
          <Text style={{fontSize: 16}}>{item.title}</Text>
        </View>
        <TouchableOpacity onPress={() => stateContext.handleDelete(index)}>
          <Image source={IconTrash} style={{height: 24, width: 24}} />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <>
      <StatusBar backgroundColor="#1297E0" barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header title="Tugas 4" />
        <View style={styles.body}>
          <Text>Todo List</Text>
          {/* Form Input */}
          <View style={styles.wrapperInput}>
            <View style={{flex: 1, marginRight: 20}}>
              <TextInput
                onFocus={onFocusForm}
                style={styles.input(border)}
                onBlur={onBlurFrom}
                placeholder="Input here"
                value={stateContext.input}
                onChangeText={(value) => stateContext.handleChangeInput(value)}
              />
            </View>
            <TouchableOpacity
              onPress={() => stateContext.addTodos()}
              style={styles.btnAdd}>
              <Image source={IconPlus} style={{height: 24, width: 24}} />
            </TouchableOpacity>
          </View>
          {/* LIST */}
          <View style={styles.wrapperList}>
            {stateContext.todos !== [] && (
              <FlatList
                data={stateContext.todos}
                renderItem={renderList}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default FormAndList;

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: '#dfe4ea',
    padding: 20,
  },
  input: (border) => ({
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
  }),
  wrapperList: {
    flex: 1,
    marginTop: 50,
  },
  btnAdd: {
    height: 50,
    width: 50,
    backgroundColor: '#1297E0',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  wrapperInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapperRowList: {
    borderWidth: 3,
    borderColor: '#CDC7C7',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
});
