import React, {useState, useContext, createContext} from 'react';
import {View, Text} from 'react-native';
import {Tugas4Provider} from '../../context/Context_Tugas4';
import FormAndList from './FormAndList';

const Tugas4 = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  handleChangeInput = (value) => {
    setInput(value);
  };

  addTodos = () => {
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();

    const today = `${date}/${month}/${year}`;

    setTodos([...todos, {title: input, date: today}]);
    setInput('');
  };

  handleDelete = (index) => {
    let newData = todos;
    newData.splice(index, 1);
    setTodos([...newData]);
  };

  return (
    <Tugas4Provider
      value={{input, todos, handleChangeInput, addTodos, handleDelete}}>
      <FormAndList />
    </Tugas4Provider>
  );
};

export default Tugas4;
