import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {colors} from '../../utils';
import {Gap, Header} from '../../components';
import {IconHome, IconPhoneGrey, IconEmailGrey} from '../../assets';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiYXhhcmFneSIsImEiOiJja2tlZjBrcTgwN3JzMnFvOTQwdGVhcGJrIn0.V_ldJkL7FAqRymnpwvixMg',
);

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const Bantuan = ({navigation}) => {
  const coordinates = [
    [107.58011, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766],
  ];

  useEffect(() => {
    getLocation();
  }, []);

  const getLocation = async () => {
    try {
      const permission = await MapboxGL.requestAndroidLocationPermissions();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <Header
          title="Donasi"
          type="btn-back"
          onPressBack={() => navigation.goBack()}
        />
        <View>
          <MapboxGL.MapView
            style={{
              height: windowHeight / 2,
              width: windowWidth,
            }}>
            <MapboxGL.UserLocation visible={true} />
            <MapboxGL.Camera followUserLocation={true} />
            <MapboxGL.PointAnnotation id={`cor-1`} coordinate={coordinates[0]}>
              <MapboxGL.Callout title={`${coordinates[0]}`} />
            </MapboxGL.PointAnnotation>
            <MapboxGL.PointAnnotation id={`cor-2`} coordinate={coordinates[1]}>
              <MapboxGL.Callout title={`${coordinates[1]}`} />
            </MapboxGL.PointAnnotation>
            <MapboxGL.PointAnnotation id={`cor-3`} coordinate={coordinates[2]}>
              <MapboxGL.Callout title={`${coordinates[2]}`} />
            </MapboxGL.PointAnnotation>
          </MapboxGL.MapView>
        </View>
        <Gap height={3} />
        <View>
          <View style={styles.box}>
            <View style={styles.wrapperIcon}>
              <IconHome />
              <Text style={styles.labelIcon}>Jakarta, Bandung, Yogyakarta</Text>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.wrapperIcon}>
              <IconEmailGrey />
              <Text style={styles.labelIcon}>cs@crowdfunding.com</Text>
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.wrapperIcon}>
              <IconPhoneGrey />
              <Text style={styles.labelIcon}>(021) 777-888</Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Bantuan;

const styles = StyleSheet.create({
  box: {
    marginTop: 2,
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  wrapperIcon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    height: 30,
    width: 30,
  },
  labelIcon: {
    marginLeft: 20,
    fontSize: 18,
  },
});
