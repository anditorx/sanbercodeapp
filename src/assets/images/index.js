import ImgAva from './avatar.png';
import ImgNoAva from './no-avatar.png';
import ImgSlide01 from './slide-01.png';
import ImgSlide02 from './slide-02.png';
import ImgSlide03 from './slide-03.png';
import ImgLogo from './logo.png';
import ImgLogoColor from './logo-color.png';
import ImgBgHeaderHome from './img-bg-header-home.png';
import ImgHeroSlider01 from './img-slider-01.jpg';
import ImgHeroSlider02 from './img-slider-02.jpg';
import ImgNoImgAvailable from './no-img-available.jpg';
export {
  ImgAva,
  ImgSlide01,
  ImgSlide02,
  ImgSlide03,
  ImgLogo,
  ImgLogoColor,
  ImgNoAva,
  ImgBgHeaderHome,
  ImgHeroSlider01,
  ImgHeroSlider02,
  ImgNoImgAvailable,
};
