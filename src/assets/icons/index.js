import IconWallet from './wallet.png';
import IconSetting from './setting.png';
import IconTerms from './term.png';
import IconHelp from './help.png';
import IconLogout from './logout.png';
import IconPlus from './plus.png';
import IconTrash from './trash.png';
import IconBackColor from './arrowBack-color.png';
import IconBackBlack from './arrowBack-black.png';
import IconBackWhite from './arrowBack-white.png';
import IconUserColor from './ic-user-color.png';
import IconKeyColor from './ic-key-color.png';
import IconEyeHide from './ic-eye-hide.png';
import IconEyeOn from './ic-eye-on.png';
import IconGoogleColor from './ic-google-color.png';
import IconFingerpringBlack from './ic-fingerprint-black.png';
import IconCameraWhite from './ic-camera-white.png';
import IconUploadWithCamera from './ic-upload-with-camera.png';
import IconEditWhite from './ic-edit-white.png';
import IconSaveWhite from './ic-save-white.png';
import IconCameraFlipBlack from './ic-camera-flip-black.png';
import IconPressPhoto from './icon-press-photo.png';
import IconEmailColor from './ic-email-color.png';
import IconHomeActive from './ic-home-color.svg';
import IconHome from './ic-home-grey.svg';
import IconProfileActive from './ic-profile-color.svg';
import IconProfile from './ic-profile-grey.svg';
import IconLikeWhite from './ic-like-white.png';
import IconTopUp from './ic-top-up.png';
import IconRiwayat from './ic-riwayat.png';
import IconMore from './ic-more.png';
import IconSaldo from './ic-saldo.png';
import IconCircleBantu from './ic-circle-bantu.png';
import IconCircleDonasi from './ic-circle-donasi.png';
import IconCircleRiwayat from './ic-circle-riwayat.png';
import IconCircleStatistik from './ic-circle-statistik.png';
import IconEmailGrey from './ic-email-grey.svg';
import IconPhoneGrey from './ic-phone.svg';
import IconChat from './ic-chat.svg';
import IconChatActive from './ic-chat-active.svg';
import IconCamera2 from './ic-camera-2.png';

export {
  IconCamera2,
  IconChat,
  IconChatActive,
  IconEmailGrey,
  IconPhoneGrey,
  IconCircleBantu,
  IconCircleDonasi,
  IconCircleRiwayat,
  IconCircleStatistik,
  IconWallet,
  IconSetting,
  IconTerms,
  IconLogout,
  IconHelp,
  IconPlus,
  IconTrash,
  IconBackColor,
  IconBackWhite,
  IconBackBlack,
  IconUserColor,
  IconKeyColor,
  IconEyeHide,
  IconEyeOn,
  IconGoogleColor,
  IconFingerpringBlack,
  IconCameraWhite,
  IconUploadWithCamera,
  IconEditWhite,
  IconSaveWhite,
  IconCameraFlipBlack,
  IconPressPhoto,
  IconEmailColor,
  IconHomeActive,
  IconHome,
  IconProfileActive,
  IconProfile,
  IconLikeWhite,
  IconTopUp,
  IconRiwayat,
  IconMore,
  IconSaldo,
};
