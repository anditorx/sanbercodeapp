import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Splashscreen,
  Intro,
  Login,
  Account,
  AccountEdit,
  Register,
  RegisterVerification,
  RegisterUpdatePassword,
  Home,
  Donasi,
  DonasiDetail,
  Payment,
  Bantuan,
  Statistic,
  Inbox,
  Riwayat,
  Bantu,
} from '../screens';
import AsyncStorage from '@react-native-async-storage/async-storage';
import configureGoogleSignIn from '../utils/configureGoogleSignIn';
import {BottomNavigator} from '../components';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={(props) => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Inbox" component={Inbox} />
      <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
  );
};

const MainNavigation = ({token}) => (
  <Stack.Navigator initialRouteName={token == null ? 'Intro' : 'MainApp'}>
    <Stack.Screen
      name="MainApp"
      component={MainApp}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
    <Stack.Screen
      name="Account"
      component={Account}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="AccountEdit"
      component={AccountEdit}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="RegisterVerification"
      component={RegisterVerification}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="RegisterUpdatePassword"
      component={RegisterUpdatePassword}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Donasi"
      component={Donasi}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="DonasiDetail"
      component={DonasiDetail}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Payment"
      component={Payment}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Bantuan"
      component={Bantuan}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Statistic"
      component={Statistic}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Inbox"
      component={Inbox}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Riwayat"
      component={Riwayat}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Bantu"
      component={Bantu}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const Routes = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
    getToken();
    configureGoogleSignIn();
  }, []);

  const getToken = async () => {
    const userToken = await AsyncStorage.getItem('token');
    setToken(userToken);
  };

  if (isLoading) {
    return <Splashscreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation token={token} />
    </NavigationContainer>
  );
};

export default Routes;
